/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.aketanawat.exhaustivesearch;

/**
 *
 * @author acer
 */
public class TestExhaustiveSearch {

    public static void main(String[] args) {
        int[] a = {1, 3, 5, 7, 10, 4, 8};
        showInput(a);
        EXS exs = new EXS(a);
        exs.process();
//        exs.getDisjointArray1();
//        System.out.println("------------------------");
//        exs.getDisjointArray2();
        exs.sum();

    }

    private static void showInput(int[] a) {
        System.out.print("Input is: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
        System.out.println("");
    }
}
